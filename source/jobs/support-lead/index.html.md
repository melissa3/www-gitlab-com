---
layout: job_page
title: "Support Lead"
---

## Responsibilities

* Interviews applicants for support engineering positions
* Continues to spend part of time resolving tickets and customer issues
* Does 1:1's with all support engineers every 1-2 weeks
* Is available for 1:1's on demand
* Maintains a written onboarding and process that is effective and supervises that its followed
* Ensures that the Support direction goals are met and match the overall team's [strategy](https://about.gitlab.com/strategy/)
* Uses various analytics to review teams's performance to help any service engineer that might be blocked
* Delivers input on promotions, function changes, demotions and firings in consultation with the CEO and VP of Engineering
* Monitors channel metrics to maintain customer and user SLAs
* Monitors channel satisfaction to improve customer experience
* Improves on the support team's process
* Ensures documentation and references are created and available to decrease the demand on support and improve onboarding

