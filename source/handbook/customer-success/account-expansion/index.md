---
layout: markdown_page
title: "Account Expansion Plan Template"
---

For each account we have identifed there is significant growth opportunity, we want to create an account expansion plan to help us realize the potential for every account.
To create an account expansion plan: 
1. Create a new issue withing the [Customer Success Issue Tracker[(https://gitlab.com/gitlab-com/customer-success/issues).  This is a private project so customer names can be mentioned.
1. Name the issue "Account Name - Expansion Plan"
1. Copy and paste the template below into the issue you just created
1. Modify the template based on your specific situation with any tasks you feel would be relevant.  This template is just a guide.
1. For any tasks you are needing help on, please add a comment noting the actual task and cc the person you are requesting help from and the ask you have of them.

```
-----------------------------
### ACCOUNT EXPANSION TEMPLATE

- [ ] Who is/are our internal champion(s)? someone who sells GitLab internally to their colleagues and management when there is no GitLab employee present.
- [ ] Have you checked version.gitlab.com to see if there are CE licenses in use there already?
- [ ] Check the Usage pings at version.gitlab.com to gather information about how the customer is using his current subscription
- [ ] Have you checked SFDC to see if there are related companies that have purchased?  
- [ ] Have you checked to see if there are users of gitlab.com with email addresses at your target's domain?  These could be champions or have logged issues with us whose state would be important to know
- [ ] Is there a reseller who has done lots of business with them in the past?  Our resellers likely have deeper relationships than we do at their customers and therefore very influential.  
- [ ] Does the target company share investors with us (Khosla, August, Joe Montana, Ashton Kutcher, etc)?  Mutual investors can be Über influential and are on our side.
- [ ] How do they make money?
- [ ] Which parts of the world do they operate in?
- [ ] What different business units exist at the company
- [ ] Record their industry within the account object of Salesforce.com
- [ ] Recorded is in their technology stack within Salesforce.com 
- [ ] What development tools within the company are provided 'as-a-service' by Engineering IT/Infrastructure and how does GitLab get on that list?
- [ ] Any executive changes at the company, company specific challenges and any relevant news events/press releases for the last year?
- [ ] Does the company have any job postings for developers?  Can learn a lot about what tools they use and where their locations are at from job postings.
- [ ] Is the company a proponent of using Open Source software? 
- [ ] Check Zendesk to see how many and what support requests the customer has submitted and how they were resolved.
- [ ] Check CE and EE issues tracker to see whether any open issues remain or (for closed ones) we can demonstrate our continued support for their needs.
- [ ] Have a clear understanding of which group/division is using GitLab, what for and who is responsible for the server. The teams may be different. Eg: a product team might be using GitLab for development but an Infrastructure team is responsible for maintaining the server.
- [ ] Determine if the team maintaining the GitLab server is some sort of shared services team. ie: can they support anyone in the entire company. If not, does such a team exist?
- [ ] What development initiatives are underway or being considered eg: code sharing / inner-sourcing; DevOps; Cloud; Agile; CI/CD? What team is or would be driving those initiatives?
- [ ] Offer a product demo highlighting new features to the existing user group and have them invite others from outside the group
- [ ] Do they understand the complete potential for both EE users and GitLab Products?  
- [ ] Have they shared the timeline they are looking at to add users? 
- [ ] Is there interest in a multi-year commitment? 
- [ ] Do you know if there is budget allocated for an expansion? If so, do you know the number? 
- [ ] Have you identified the right DM's for expansion? Who is all involved in this decision? 
- [ ] Introductions by your current contact to other groups or the right Decision Maker underway? If not, what do you need to do to get this? 
- [ ] Do you have a meeting scheduled with the right Decision Maker? If not, do you have a timeline for the right time to discuss with them established? 
- [ ] Is there a current pilot underway that will be influential in the potential expansion? 
- [ ] If there is a pilot, have you identified the key results [success criteria - use](https://about.gitlab.com/handbook/sales/POC/) needed from it for expansion to happen? 
- [ ] Do you need to send any trial license keys to other groups, or trial's for GitLab Products? Use [POC template](https://about.gitlab.com/handbook/sales/POC/)
- [ ] If so, did you set a meeting 2 weeks into it to hear about their experience so far? 
- [ ] Have you found out what potential deal blockers are, and if there are reasons they wouldn't expand, what would those be? 
- [ ] Plan to resolve concerns shared with prospect?
- [ ] After your meeting with the DM/DM's, what is your next step after sending them a follow up? 
- [ ] Next meeting date confirmed ?
- [ ] Have you put together a proposal and included options that meet their needs you discovered above? i.e. multi-year prepaid, ramped up, etc. 
- [ ] Does your contact need any assistance presenting your proposal or need materials from you in order to present GitLab in the best way possible? 
- [ ] Do you have a date to work towards to have the purchase complete? 
- [ ] Opportunity created with type called new division - existing business.  Add link?
```